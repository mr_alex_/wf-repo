class StringConstants {
  static const String windCaps = 'WIND';
  static const String feelsLikeCaps = 'FEELS LIKE';
  static const String humidityCaps = 'HUMIDITY';
  static const String pressureCaps = 'PRESSURE';

  static const String today = 'Today';
  static const String next7days = 'Next 7 days';

  static const String metersPerSecondShort = 'm/s';
  static const String hectoPascalShort = 'hPa';

  static const String celsiusDegreeSymbol = '°';
  static const String percentSymbol = '%';
}