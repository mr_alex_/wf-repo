class AppConstants {
  static const String weatherApiKey = 'f8961b1d3aa2ce0a20aa6c27805bcef5';

  static const String weatherIconUrl = 'http://openweathermap.org/img/wn/';

  //TODO: remove this variable and use an information from the settings page
  static const String currentCityForRequests = 'Dnipro';
}