import 'package:flutter/material.dart';

class WeatherPageConstants {
  static const Color lightPageBackgroundColor = Color(0xFFFFFFFF);
  static const Color contentBackgroundColor = Color(0xFF467ffb);
  static const Color darkPageBackgroundColor = Color(0xFF202059);
}

class WeekForecastPageConstants {
  static const Color lightPageBackgroundColor = Color(0xFF467ffb);
  static const Color darkPageBackgroundColor = Color(0xFF202059);
}