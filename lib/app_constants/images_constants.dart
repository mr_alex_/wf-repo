class ImagesConstants {
  static const String cloudy = 'assets/images/046-cloudy.svg';
  static const String wind = 'assets/images/005-wind-sign.svg';
  static const String thermometer = 'assets/images/006-thermometer-3.svg';
  static const String humidity = 'assets/images/027-humidity.svg';
  static const String barometer = 'assets/images/050-barometer.svg';
  static const String cloud = 'assets/images/047-cloud.svg';
  static const String sun = 'assets/images/009-sun-1.svg';
  static const String rain = 'assets/images/023-rain.svg';
  static const String storm = 'assets/images/012-storm-4.svg';
}