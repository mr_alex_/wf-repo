class RoutesConstants {
  static const String weatherPage = '/';
  static const String weekForecastPage = 'week_forecast';
  static const String settingsPage = 'settings';
}