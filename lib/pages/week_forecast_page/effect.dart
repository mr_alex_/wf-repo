import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' as material;
import 'package:weather_app/helpers/weather_model_helper.dart';
import 'package:weather_app/models/coordinate_model.dart';
import 'package:weather_app/services/weather_service.dart';

import 'action.dart';
import 'state.dart';

Effect<WeekForecastState> buildEffect() {
  return combineEffects(<Object, Effect<WeekForecastState>>{
    WeekForecastAction.backButtonTapped: _onBackButtonTapped,
    WeekForecastAction.getDailyForecast: _onGetDailyForecast,
  });
}

void _onBackButtonTapped(Action action, Context<WeekForecastState> ctx) {
  material.Navigator.pop(ctx.context);
}

void _onGetDailyForecast(Action action, Context<WeekForecastState> ctx) async {
  CoordinateModel coords = action.payload as CoordinateModel;

  if (coords != null) {
    var jweather = await WeatherService.getDailyWeather(coords);

    if (jweather != null) {
      var weatherModelList = WeatherModelHelper.dailyListFromJson(jweather);
      ctx.dispatch(WeekForecastActionCreator.onUpdateDailyForecast(weatherModelList));
    }
  }
}