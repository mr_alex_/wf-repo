import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<WeekForecastState> buildReducer() {
  return asReducer(
    <Object, Reducer<WeekForecastState>>{
      WeekForecastAction.updateDailyForecast: _onUpdateDailyForecast
    },
  );
}

WeekForecastState _onUpdateDailyForecast(WeekForecastState state, Action action) {
  if (action.payload != null) {
    final WeekForecastState newState = state.clone();

    newState.dailyWeatherList = action.payload;
    newState.dailyForecastLoaded = true;

    return newState;
  }

  return state;
}