import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/models/coordinate_model.dart';
import 'package:weather_app/models/daily_weather_model.dart';

class WeekForecastState implements Cloneable<WeekForecastState> {
  final CoordinateModel coordinates = CoordinateModel(34.98, 48.45);

  final String pageTitle = 'Dnipro, UA';

  List<DailyWeatherModel> dailyWeatherList = [];
  bool dailyForecastLoaded = false;

  @override
  WeekForecastState clone() {
    return WeekForecastState()
      ..dailyWeatherList = this.dailyWeatherList;
  }
}

WeekForecastState initState(Map<String, dynamic> args) {
  return WeekForecastState();
}
