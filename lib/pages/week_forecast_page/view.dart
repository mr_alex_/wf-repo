import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:weather_app/app_constants/color_constants.dart';
import 'package:weather_app/app_constants/images_constants.dart';
import 'package:weather_app/app_constants/string_constatns.dart';
import 'package:weather_app/models/daily_weather_model.dart';
import 'package:weather_app/pages/week_forecast_page/action.dart';
import 'package:weather_app/pages/week_forecast_page/state.dart';

Widget buildView(WeekForecastState state, Dispatch dispatch, ViewService viewService) {
  var forecastList = state.dailyWeatherList as List<DailyWeatherModel>;

  if (!state.dailyForecastLoaded) {
    dispatch(WeekForecastActionCreator.onGetDailyForecast(state.coordinates));
  }

  return Scaffold(
    backgroundColor: WeekForecastPageConstants.lightPageBackgroundColor,
    appBar: AppBar(
        shadowColor: Colors.transparent,
        backgroundColor: WeekForecastPageConstants.lightPageBackgroundColor,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white
          ),
          onPressed: () {
            dispatch(WeekForecastActionCreator.onBackButtonTapped());
          },
        ),
        title: Text(
          state.pageTitle,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 22,
            color: Colors.white,
          )
        )
    ),
    body: state.dailyForecastLoaded ?
      ListView(
      children: [
        //next 7 days header
        Container(
          margin: EdgeInsets.only(left: 20, top: 30, bottom: 25),
          child: Text(
              StringConstants.next7days,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 28,
                  fontWeight: FontWeight.w500
              )
          ),
        ),
        //weather forecast
        Column(
          children: List.generate(
            forecastList?.length,
            (index) {
            return Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //icon
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Image.network(
                      forecastList[index].imageUrl,
                      scale: 1.5
                    )
                  ),
                  //day
                  Text(
                    forecastList[index].stringDateValue,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                      color: Colors.white,
                    )
                  ),
                  //temperature
                  Container(
                    margin: EdgeInsets.only(right: 30),
                    child: RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                              text: '${forecastList[index].temperatureMax}',
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                                color: Colors.white,
                              )
                          ),
                          TextSpan(
                              text: ' / ',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 20,
                                color: Colors.white,
                              )
                          ),
                          TextSpan(
                              text: '${forecastList[index].temperatureMin}${StringConstants.celsiusDegreeSymbol}',
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 20,
                                color: Colors.white,
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
        )
      ],
    ) :
      Center(
        child: CircularProgressIndicator(backgroundColor: Colors.white,)
      )
  );
}
