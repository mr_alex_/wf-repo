import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class WeekForecastPage extends Page<WeekForecastState, Map<String, dynamic>> {
  WeekForecastPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<WeekForecastState>(
                adapter: null,
                slots: <String, Dependent<WeekForecastState>>{
                }),
            middleware: <Middleware<WeekForecastState>>[
            ],);

}
