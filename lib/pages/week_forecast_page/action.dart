import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/models/coordinate_model.dart';
import 'package:weather_app/models/daily_weather_model.dart';

enum WeekForecastAction {
  backButtonTapped,
  getDailyForecast,
  updateDailyForecast
}

class WeekForecastActionCreator {
  static Action onGetDailyForecast(CoordinateModel coords) {
    return Action(WeekForecastAction.getDailyForecast, payload: coords);
  }

  static Action onUpdateDailyForecast(List<DailyWeatherModel> weatherModellist) {
    return Action(WeekForecastAction.updateDailyForecast, payload: weatherModellist);
  }

  static Action onBackButtonTapped() {
    return const Action(WeekForecastAction.backButtonTapped);
  }
}
