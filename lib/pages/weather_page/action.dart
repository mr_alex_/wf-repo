import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/app_constants/app_constants.dart';
import 'package:weather_app/models/coordinate_model.dart';
import 'package:weather_app/models/current_weather_model.dart';
import 'package:weather_app/models/hourly_weather_model.dart';

enum WeatherAction {
  getCurrentWeatherData,
  updateCurrentWeather,
  getHourlyWeatherData,
  updateHourlyWeather,
  weatherDataLoaded,
  hourlyWeatherDataLoaded
}

class WeatherActionCreator {
  static Action onGetCurrentWeatherData({ String city = AppConstants.currentCityForRequests }) {
    return Action(WeatherAction.getCurrentWeatherData, payload: city);
  }

  static Action onUpdateCurrentWeather(CurrentWeatherModel weatherModel) {
    return Action(WeatherAction.updateCurrentWeather, payload: weatherModel);
  }

  static Action onGetHourlyWeatherData(CoordinateModel coords) {
    return Action(WeatherAction.getHourlyWeatherData, payload: coords);
  }

  static Action onUpdateHourlyWeather(List<HourlyWeatherModel> hourlyWeatherList) {
    return Action(WeatherAction.updateHourlyWeather, payload: hourlyWeatherList);
  }

  static Action onWeatherDataLoaded(bool wasLoaded) {
    return Action(WeatherAction.weatherDataLoaded, payload: wasLoaded);
  }

  static Action onHourlyWeatherDataLoaded(bool wasLoaded) {
    return Action(WeatherAction.weatherDataLoaded, payload: wasLoaded);
  }
}
