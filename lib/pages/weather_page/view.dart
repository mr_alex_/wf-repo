import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:weather_app/app_constants/connectors_constants.dart';
import 'package:weather_app/pages/weather_page/action.dart';

import 'state.dart';

Widget buildView(WeatherState state, Dispatch dispatch, ViewService viewService) {
  var theme = Theme.of(viewService.context);
  var weatherModel = state.currentWeatherState.weatherModel;

  //loading current weather
  if (!state.currentWeatherDataLoaded) {
    dispatch(WeatherActionCreator.onGetCurrentWeatherData());
  }
  
  //loading hourly weather
  if (state.currentWeatherDataLoaded && !state.hourlyWeatherDataLoaded) {
    dispatch(WeatherActionCreator.onGetHourlyWeatherData(weatherModel.coordinates));
  }

  return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu, size: 33),
          onPressed: () {},
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.more_vert, size: 33),
            onPressed: () {},
          )
        ],
      ),
      body: state.currentWeatherDataLoaded && state.hourlyWeatherDataLoaded
          ? Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      //city and country
                      Container(
                        margin: EdgeInsets.only(top: 25, bottom: 25),
                        child: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: '${weatherModel.city},',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25,
                                      color: theme.textTheme.headline1.color
                                          .withOpacity(0.9))),
                              TextSpan(
                                  text: ' ${weatherModel.country}',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 22,
                                    color: theme.textTheme.headline1.color
                                        .withOpacity(0.7),
                                  )),
                            ],
                          ),
                        ),
                      ),

                      //reload button
                      IconButton(
                        icon: Icon(
                          Icons.autorenew,
                          size: 25
                        ),
                        onPressed: () {}
                      )
                    ],
                  ),

                  //weather content
                  viewService.buildComponent(ConnectorKeys.currentWeather),

                  //hourly weather
                  viewService.buildComponent(ConnectorKeys.hourlyWeather),
                ],
              ))
          : Center(child: CircularProgressIndicator())
  );
}
