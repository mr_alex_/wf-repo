import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/pages/weather_page/components/current_weather/state.dart';
import 'package:weather_app/pages/weather_page/components/hourly_weather/state.dart';

class WeatherState implements Cloneable<WeatherState> {
  CurrentWeatherState currentWeatherState;
  HourlyWeatherState hourlyWeatherState;

  bool currentWeatherDataLoaded = false;
  bool hourlyWeatherDataLoaded = false;

  @override
  WeatherState clone() {
    return WeatherState()
      ..currentWeatherState = this.currentWeatherState
      ..hourlyWeatherState = this.hourlyWeatherState
      ..currentWeatherDataLoaded = this.currentWeatherDataLoaded
      ..hourlyWeatherDataLoaded = this.hourlyWeatherDataLoaded;
  }
}

WeatherState initState(Map<String, dynamic> args)  {
  var state = WeatherState()
    ..currentWeatherState = CurrentWeatherState()
    ..hourlyWeatherState = HourlyWeatherState();

  return state;
}
