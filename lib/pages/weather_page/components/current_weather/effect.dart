import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Effect<CurrentWeatherState> buildEffect() {
  return combineEffects(<Object, Effect<CurrentWeatherState>>{
    CurrentWeatherAction.action: _onAction,
  });
}

void _onAction(Action action, Context<CurrentWeatherState> ctx) async {
}
