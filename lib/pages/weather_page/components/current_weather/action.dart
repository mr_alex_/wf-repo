import 'package:fish_redux/fish_redux.dart';

enum CurrentWeatherAction { action }

class CurrentWeatherActionCreator {
  static Action onGetCurrentWeatherData(String city) {
    return const Action(CurrentWeatherAction.action);
  }
}
