import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/pages/weather_page/components/current_weather/effect.dart';

import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class CurrentWeatherComponent extends Component<CurrentWeatherState> {
  CurrentWeatherComponent()
      : super(
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<CurrentWeatherState>(
                adapter: null,
                slots: <String, Dependent<CurrentWeatherState>>{
                }),
  );
}
