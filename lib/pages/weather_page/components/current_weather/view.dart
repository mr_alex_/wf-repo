import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:weather_app/app_constants/images_constants.dart';
import 'package:weather_app/app_constants/string_constatns.dart';
import 'package:weather_app/models/current_weather_model.dart';
import 'package:weather_app/pages/weather_page/components/current_weather/action.dart';

import '../../../../app_constants/string_constatns.dart';
import 'state.dart';

Widget buildView(CurrentWeatherState state, Dispatch dispatch, ViewService viewService) {
  var theme =  Theme.of(viewService.context);
  var weather = state.weatherModel as CurrentWeatherModel;

  return Container(
    width: MediaQuery.of(viewService.context).size.width,
    decoration: BoxDecoration(
      color: theme.backgroundColor,
      borderRadius: BorderRadius.all(const Radius.circular(15.0)),
    ),
    child: Column(
      children: [
        //main info
        Container(
          margin: EdgeInsets.only(top: 30),
          child: Image.network(
            weather.imageUrl,
            scale: 0.6,
            width: 140,
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 5),
          child: Text(
            weather.description,
            style: TextStyle(
              color: theme.textTheme.headline2.color,
              fontSize: 30,
              fontWeight: FontWeight.w600
            )
          ),
        ),
        Text(
          weather?.stringDateTimeValue,
          style: TextStyle(
            color: theme.textTheme.headline5.color,
            fontSize: 18,
            fontWeight: FontWeight.w400
          )
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 20),
          child: Text(
            "${weather?.temperature}${StringConstants.celsiusDegreeSymbol}",
            style: TextStyle(
              color: theme.textTheme.bodyText1.color,
              fontSize: 50,
              fontWeight: FontWeight.w600
            )
          ),
        ),

        //separator
        Container(
          width: MediaQuery.of(viewService.context).size.width,
          height: 1,
          color: Colors.white.withOpacity(0.9)
        ),

        //additional info box #1
        Container(
          margin: EdgeInsets.only(top: 15, bottom: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              //wind
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 15, right: 20),
                    child: SvgPicture.asset(
                      ImagesConstants.wind,
                      width: 40,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Text(
                          StringConstants.windCaps,
                          style: TextStyle(
                            color: theme.textTheme.headline5.color,
                            fontSize: 17,
                            fontWeight: FontWeight.w500
                          )
                        ),
                      ),
                      Text(
                        "${weather?.windSpeed} ${StringConstants.metersPerSecondShort}",
                        style: TextStyle(
                          color: theme.textTheme.headline5.color,
                          fontSize: 17,
                          fontWeight: FontWeight.w700
                        )
                      )
                    ],
                  )
                ],
              ),
              //temperature
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 60, right: 15),
                    child: SvgPicture.asset(
                      ImagesConstants.thermometer,
                      width: 40,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Text(
                          StringConstants.feelsLikeCaps,
                          style: TextStyle(
                            color: theme.textTheme.headline5.color,
                            fontSize: 17,
                            fontWeight: FontWeight.w500
                          )
                        ),
                      ),
                      Text(
                        "${weather?.feelsLike}${StringConstants.celsiusDegreeSymbol}",
                        style: TextStyle(
                          color: theme.textTheme.headline5.color,
                          fontSize: 17,
                          fontWeight: FontWeight.w700
                        )
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),

        //separator
        Container(
          width: MediaQuery.of(viewService.context).size.width,
          height: 1,
          color: Colors.white.withOpacity(0.9)
        ),

        //additional info box #2
        Container(
          margin: EdgeInsets.symmetric(vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              //humidity
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 15, right: 20),
                    child: SvgPicture.asset(
                      ImagesConstants.humidity,
                      width: 40,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Text(
                          StringConstants.humidityCaps,
                          style: TextStyle(
                            color: theme.textTheme.headline5.color,
                            fontSize: 17,
                            fontWeight: FontWeight.w500
                          )
                        ),
                      ),
                      Text(
                        "${weather?.humidity} ${StringConstants.percentSymbol}",
                        style: TextStyle(
                          color: theme.textTheme.headline5.color,
                          fontSize: 17,
                          fontWeight: FontWeight.w700
                        )
                      )
                    ],
                  )
                ],
              ),
              //pressure
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 30, right: 15),
                    child: SvgPicture.asset(
                      ImagesConstants.barometer,
                      width: 35,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Text(
                          StringConstants.pressureCaps,
                          style: TextStyle(
                            color: theme.textTheme.headline5.color,
                            fontSize: 17,
                            fontWeight: FontWeight.w500
                          )
                        ),
                      ),
                      Text(
                        "${weather?.pressure} ${StringConstants.hectoPascalShort}",
                        style: TextStyle(
                          color: theme.textTheme.headline5.color,
                          fontSize: 17,
                          fontWeight: FontWeight.w700
                        )
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ],
    )
  );
}
