import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/models/current_weather_model.dart';

class CurrentWeatherState implements Cloneable<CurrentWeatherState> {
  CurrentWeatherModel _weatherModel;

  set weatherModel (CurrentWeatherModel value) {
    if (value != null) {
      _weatherModel = value;
    }
    else {
      _weatherModel = CurrentWeatherModel();
    }
  }

  CurrentWeatherModel get weatherModel => _weatherModel;

  @override
  CurrentWeatherState clone() {
    return CurrentWeatherState()
      .._weatherModel = this._weatherModel;
  }
}

CurrentWeatherState initState(Map<String, dynamic> args) {
  return CurrentWeatherState();
}