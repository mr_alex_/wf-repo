import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<CurrentWeatherState> buildReducer() {
  return asReducer(
    <Object, Reducer<CurrentWeatherState>>{
      CurrentWeatherAction.action: _onAction,
    },
  );
}

CurrentWeatherState _onAction(CurrentWeatherState state, Action action) {
}
