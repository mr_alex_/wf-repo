import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/models/coordinate_model.dart';
import 'package:weather_app/models/hourly_weather_model.dart';

enum HourlyWeatherAction {
  showWeeklyWeather,
  getHourlyWeatherData,
  updateHourlyWeather,
}

class HourlyWeatherActionCreator {
  static Action onShowWeeklyWeather() {
    return const Action(HourlyWeatherAction.showWeeklyWeather);
  }

  static Action onGetHourlyWeatherData(CoordinateModel coords) {
    return Action(HourlyWeatherAction.getHourlyWeatherData, payload: coords);
  }

  static Action onUpdateHourlyWeather(List<HourlyWeatherModel> hourlyWeatherList) {
    return Action(HourlyWeatherAction.updateHourlyWeather, payload: hourlyWeatherList);
  }
}
