import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:weather_app/app_constants/color_constants.dart';
import 'package:weather_app/app_constants/images_constants.dart';
import 'package:weather_app/app_constants/string_constatns.dart';
import 'package:weather_app/models/hourly_weather_model.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(HourlyWeatherState state, Dispatch dispatch, ViewService viewService) {
  var theme = Theme.of(viewService.context);
  var hourlyList = state.hourlyWeatherlist as List<HourlyWeatherModel>;;

  return Column(
    children: [
      //today and next 7 days
      Container(
        margin: EdgeInsets.only(top: 30, bottom: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              StringConstants.today,
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w700,
                color: theme.textTheme.headline3.color
              )
            ),
            GestureDetector(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                      StringConstants.next7days,
                      style: TextStyle(
                          color: theme.textTheme.headline3.color.withOpacity(0.8),
                          fontSize: 20,
                          fontWeight: FontWeight.w600
                      )
                  ),
                  Icon(
                      Icons.arrow_forward_ios,
                      color: theme.iconTheme.color.withOpacity(0.8),
                      size: 22
                  ),
                ],
              ),
              onTap: () {
                dispatch(HourlyWeatherActionCreator.onShowWeeklyWeather());
              },
            )
          ],
        ),
      ),

      //weather info list
      Container(
        height: 160,
        margin: EdgeInsets.only(bottom: 15),
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: hourlyList.length,
            itemBuilder: (context, index) {
              return Container(
                  margin: EdgeInsets.only(right: 10),
                  decoration: BoxDecoration(
                      color: WeatherPageConstants.contentBackgroundColor,
                      borderRadius: BorderRadius.all(const Radius.circular(15.0)),
                      border: Border.all(
                        color: theme.cardColor.withOpacity(0.3),
                      ),
                  ),
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                    child: Column(
                      children: [
                        Text(
                            hourlyList[index].stringHoursMinutesValue,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w600
                            )
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 15),
                          child: Image.network(
                            hourlyList[index].imageUrl,
                            width: 50,
                          )
                        ),
                        Text(
                            "${hourlyList[index].temperature}${StringConstants.celsiusDegreeSymbol}",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontWeight: FontWeight.w800
                            )
                        )
                      ],
                    ),
                  )
              );
            }
        ),
      )
    ],
  );
}
