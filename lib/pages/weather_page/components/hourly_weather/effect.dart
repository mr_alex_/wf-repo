import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' as material;
import 'package:weather_app/app_constants/routes_constants.dart';

import 'action.dart';
import 'state.dart';

Effect<HourlyWeatherState> buildEffect() {
  return combineEffects(<Object, Effect<HourlyWeatherState>>{
    HourlyWeatherAction.showWeeklyWeather: _onShowWeeklyWeather,
  });
}

void _onShowWeeklyWeather(Action action, Context<HourlyWeatherState> ctx) async {
  material.Navigator.pushNamed(ctx.context, RoutesConstants.weekForecastPage);
}
