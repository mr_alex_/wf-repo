import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/models/hourly_weather_model.dart';

class HourlyWeatherState implements Cloneable<HourlyWeatherState> {
  List<HourlyWeatherModel> hourlyWeatherlist = [];

  @override
  HourlyWeatherState clone() {
    return HourlyWeatherState()
      ..hourlyWeatherlist = this.hourlyWeatherlist;
  }
}

HourlyWeatherState initState(Map<String, dynamic> args) {
  return HourlyWeatherState();
}
