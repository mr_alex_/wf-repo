import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' as material;
import 'package:weather_app/app_constants/routes_constants.dart';
import 'package:weather_app/helpers/weather_model_helper.dart';
import 'package:weather_app/models/coordinate_model.dart';
import 'package:weather_app/models/current_weather_model.dart';
import 'package:weather_app/services/weather_service.dart';
import 'action.dart';
import 'state.dart';

Effect<WeatherState> buildEffect() {
  return combineEffects(<Object, Effect<WeatherState>>{
    WeatherAction.getCurrentWeatherData: _onGetCurrentWeatherData,
    WeatherAction.getHourlyWeatherData: _onGetHourlyWeatherData
  });
}

void _onGetCurrentWeatherData(Action action, Context<WeatherState> ctx) async {
  String city = action.payload.toString();
  bool canRequest = city != null && city.isNotEmpty;

  if (canRequest) {
    var jweather = await WeatherService.getCurrentWeather(city);

    if (jweather != null) {
      var weatherModel = CurrentWeatherModel.fromJson(jweather);
      ctx.dispatch(WeatherActionCreator.onUpdateCurrentWeather(weatherModel));
    }
  }
}

void _onGetHourlyWeatherData(Action action, Context<WeatherState> ctx) async {
  CoordinateModel coords = action.payload as CoordinateModel;

  if (coords != null) {
    var jweather = await WeatherService.getHourlyWeather(coords);

    if (jweather != null) {
      var weatherModelList = WeatherModelHelper.hourlyListFromJson(jweather);
      ctx.dispatch(WeatherActionCreator.onUpdateHourlyWeather(weatherModelList));
    }
  }
}