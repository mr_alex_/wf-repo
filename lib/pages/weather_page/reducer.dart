import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/models/current_weather_model.dart';

import 'action.dart';
import 'state.dart';

Reducer<WeatherState> buildReducer() {
  return asReducer(
    <Object, Reducer<WeatherState>>{
      WeatherAction.updateCurrentWeather: _onUpdateCurrentWeather,
      WeatherAction.updateHourlyWeather: _onUpdateHourlyWeather,
      WeatherAction.weatherDataLoaded: _onWeatherDataLoaded,
      WeatherAction.hourlyWeatherDataLoaded: _onHourlyWeatherDataLoaded
    },
  );
}

WeatherState _onUpdateCurrentWeather(WeatherState state, Action action) {
  bool canUpdateState = action.payload != null &&
      action.payload is CurrentWeatherModel;

  if (canUpdateState) {
    final WeatherState newState = state.clone();

    newState.currentWeatherState.weatherModel = action.payload;
    newState.currentWeatherDataLoaded = true;

    return newState;
  }

  return state;
}

WeatherState _onUpdateHourlyWeather(WeatherState state, Action action) {
  if (action.payload != null) {
    final WeatherState newState = state.clone();

    newState.hourlyWeatherState.hourlyWeatherlist = action.payload;
    newState.hourlyWeatherDataLoaded = true;

    return newState;
  }

  return state;
}

WeatherState _onWeatherDataLoaded(WeatherState state, Action action) {
  final WeatherState newState = state.clone();
  newState.currentWeatherDataLoaded = action.payload;

  return newState;
}

WeatherState _onHourlyWeatherDataLoaded(WeatherState state, Action action) {
  final WeatherState newState = state.clone();
  newState.hourlyWeatherDataLoaded = action.payload;

  return newState;
}