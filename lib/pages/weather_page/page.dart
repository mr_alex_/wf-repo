import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/app_constants/connectors_constants.dart';
import 'package:weather_app/pages/weather_page/components/current_weather/component.dart';
import 'package:weather_app/pages/weather_page/components/hourly_weather/component.dart';
import 'package:weather_app/pages/weather_page/connectors/current_weather_connector.dart';
import 'package:weather_app/pages/weather_page/connectors/hourly_weather_connector.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class WeatherPage extends Page<WeatherState, Map<String, dynamic>> {
  WeatherPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<WeatherState>(
                adapter: null,
                slots: <String, Dependent<WeatherState>> {
                  ConnectorKeys.currentWeather :
                    CurrentWeatherConnector() + CurrentWeatherComponent(),
                  ConnectorKeys.hourlyWeather :
                    HourlyWeatherConnector() + HourlyWeatherComponent()
                }),
            middleware: <Middleware<WeatherState>> [
            ],);

}
