import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/pages/weather_page/components/current_weather/state.dart';
import 'package:weather_app/pages/weather_page/state.dart';

class CurrentWeatherConnector extends ConnOp<WeatherState, CurrentWeatherState> {
  @override
  CurrentWeatherState get(WeatherState weatherState) => weatherState.currentWeatherState;

  @override
  void set(WeatherState weatherState, CurrentWeatherState currentWeatherSubState) =>
      weatherState.currentWeatherState = currentWeatherSubState;
}