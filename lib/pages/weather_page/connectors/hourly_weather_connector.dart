import 'package:fish_redux/fish_redux.dart';
import 'package:weather_app/pages/weather_page/components/hourly_weather/state.dart';
import 'package:weather_app/pages/weather_page/state.dart';

class HourlyWeatherConnector extends ConnOp<WeatherState, HourlyWeatherState> {
  @override
  HourlyWeatherState get(WeatherState weatherState) => weatherState.hourlyWeatherState;

  @override
  void set(WeatherState weatherState, HourlyWeatherState hourlyWeatherSubState) =>
      weatherState.hourlyWeatherState = hourlyWeatherSubState;
}