import 'package:intl/intl.dart';
import 'package:weather_app/app_constants/app_constants.dart';

class DailyWeatherModel {
  String imageUrl;
  int temperatureMax;
  int temperatureMin;
  DateTime dateTime;

  DailyWeatherModel.fromJson(Map<String, dynamic> json)
    : temperatureMax = json['temp']['max'].toInt(),
      temperatureMin = json['temp']['min'].toInt(),
      dateTime = DateTime.fromMillisecondsSinceEpoch(json['dt'] * 1000)
  {
    imageUrl = '${AppConstants.weatherIconUrl}${json['weather'][0]['icon']}@2x.png';
  }
}

extension DailyWeatherModelExtension on DailyWeatherModel {
  String get stringDateValue {
    return DateFormat.MMMEd().format(dateTime).toString();
  }
}