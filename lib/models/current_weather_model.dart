import 'package:intl/intl.dart';
import 'package:weather_app/app_constants/app_constants.dart';
import 'package:weather_app/models/coordinate_model.dart';

class CurrentWeatherModel {
  String imageUrl;
  String city;
  String country;
  String description;
  int windSpeed;
  int temperature;
  int feelsLike;
  int pressure;
  int humidity;

  CoordinateModel coordinates;
  DateTime dateTime;

  CurrentWeatherModel();

  CurrentWeatherModel.fromJson(Map<String, dynamic> json)
      : city = json['name'],
        country = json['sys']['country'],
        description = json['weather'][0]['main'],
        windSpeed = json['wind']['speed'].toInt(),
        temperature = json['main']['temp'].toInt(),
        feelsLike = json['main']['feels_like'].toInt(),
        pressure = json['main']['pressure'],
        humidity = json['main']['humidity'],
        dateTime = DateTime.fromMillisecondsSinceEpoch(json['dt'] * 1000)
  {
    imageUrl = '${AppConstants.weatherIconUrl}${json['weather'][0]['icon']}@2x.png';
    coordinates = CoordinateModel.fromJson(json['coord']);
  }
}

extension CurrentWeatherModelExtension on CurrentWeatherModel {
  String get stringDateTimeValue {
    return DateFormat.MMMMEEEEd().format(dateTime).toString();
  }
}