import 'package:intl/intl.dart';
import 'package:weather_app/app_constants/app_constants.dart';

class HourlyWeatherModel {
  String imageUrl;
  String description;
  int temperature;
  DateTime currentDate;

  HourlyWeatherModel();

  HourlyWeatherModel.fromJson(Map<String, dynamic> json)
      : description = json['weather'][0]['main'],
        temperature = json['temp'].toInt(),
        currentDate = DateTime.fromMillisecondsSinceEpoch(json['dt'] * 1000)
  {
    imageUrl = '${AppConstants.weatherIconUrl}${json['weather'][0]['icon']}@2x.png';
  }
}

extension HourlyWeatherModelExtension on HourlyWeatherModel {
  String get stringHoursMinutesValue {
    return DateFormat.Hm().format(currentDate).toString();
  }
}