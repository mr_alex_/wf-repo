class CoordinateModel {
  double longitude;
  double latitude;

  CoordinateModel(this.longitude, this.latitude);

  CoordinateModel.fromJson(Map<String, dynamic> json)
    : longitude = json['lon'],
      latitude = json['lat'];
}