import 'package:flutter/material.dart';
import 'package:weather_app/app_constants/color_constants.dart';

class AppThemes {
  static ThemeData get lightTheme => ThemeData(
    appBarTheme: AppBarTheme(
      color: WeatherPageConstants.lightPageBackgroundColor,
      shadowColor: Colors.transparent,
      iconTheme: IconThemeData(
        color: Colors.black87
      ),
      actionsIconTheme: IconThemeData(
          color: Colors.black87
      )
    ),
    scaffoldBackgroundColor: WeatherPageConstants.lightPageBackgroundColor,
    textTheme: TextTheme(
      headline1: TextStyle(
        color: Colors.black,
      ),
      headline2: TextStyle(
        color: Colors.white,
      ),
      headline3: TextStyle(
        color: Colors.black
      ),
      headline4: TextStyle(
        color: Colors.black
      ),
      headline5: TextStyle(
        color: Colors.white,
      ),
      bodyText1: TextStyle(
        color: Colors.white,
      )
    ),
    iconTheme: IconThemeData(
      color: Colors.black
    ),
    cardColor: Colors.black,
    backgroundColor: WeatherPageConstants.contentBackgroundColor,
  );

  static ThemeData get darkTheme => ThemeData(
    appBarTheme: AppBarTheme(
        color: WeatherPageConstants.darkPageBackgroundColor,
        shadowColor: Colors.transparent,
        iconTheme: IconThemeData(
            color: Colors.white60
        ),
        actionsIconTheme: IconThemeData(
            color: Colors.white60
        )
    ),
    scaffoldBackgroundColor: WeatherPageConstants.darkPageBackgroundColor,
    textTheme: TextTheme(
        headline1: TextStyle(
          color: Colors.white60,
        ),
        headline2: TextStyle(
          color: Colors.white60,
        ),
        headline3: TextStyle(
            color: Colors.white60
        ),
        headline4: TextStyle(
            color: Colors.white60
        ),
        headline5: TextStyle(
          color: Colors.white60,
        ),
        bodyText1: TextStyle(
          color: Colors.white60,
        )
    ),
    iconTheme: IconThemeData(
        color: Colors.white60
    ),
    cardColor: Colors.white,
    backgroundColor: WeatherPageConstants.contentBackgroundColor.withOpacity(0.3),
  );
}