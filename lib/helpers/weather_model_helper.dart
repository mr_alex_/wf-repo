import 'package:weather_app/models/daily_weather_model.dart';
import 'package:weather_app/models/hourly_weather_model.dart';

class WeatherModelHelper {
  static List<HourlyWeatherModel> hourlyListFromJson(Map<String, dynamic> json) {
    var jhourlyList = json['hourly'] as List;

    List<HourlyWeatherModel> hourlyList;

    if (jhourlyList != null) {
      hourlyList =
        jhourlyList.map((jhourlyItem) => HourlyWeatherModel.fromJson(jhourlyItem))
        .toList();

      //api returns 48 items (48 hours) and we should remove some items from the list
      hourlyList.removeWhere(
        (element) => element.currentDate.day != DateTime.now().day
      );
    }

    return hourlyList;
  }

  static List<DailyWeatherModel> dailyListFromJson(Map<String, dynamic> json) {
    var jdailyList = json['daily'] as List;

    List<DailyWeatherModel> dailyList;

    if (jdailyList != null) {
      dailyList =
        jdailyList.map((jdailyItem) => DailyWeatherModel.fromJson(jdailyItem))
        .toList();
    }

    //we should remove the forecast for the current day from the list
    dailyList.removeAt(0);

    return dailyList;
  }
}