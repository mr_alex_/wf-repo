import 'package:flutter/material.dart';
import 'package:weather_app/app_constants/routes_constants.dart';
import 'package:weather_app/app_themes.dart';
import 'package:weather_app/pages/week_forecast_page/page.dart';
import 'package:weather_app/pages/weather_page/page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather Forecast',
      theme: AppThemes.lightTheme,
      debugShowCheckedModeBanner: false,
      initialRoute: RoutesConstants.weatherPage,
      routes: {
        RoutesConstants.weatherPage : (context) => WeatherPage().buildPage(null),
        RoutesConstants.weekForecastPage : (context) => WeekForecastPage().buildPage(null),
        RoutesConstants.settingsPage : (context) => null
      },
    );
  }
}
