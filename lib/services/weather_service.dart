import 'dart:convert';

import 'package:http/http.dart';
import 'package:weather_app/app_constants/app_constants.dart';
import 'package:weather_app/models/coordinate_model.dart';

class WeatherService {
  static Future<Map<String, dynamic>> getCurrentWeather(String cityName) async
  {
   final String url =
       'http://api.openweathermap.org/data/2.5/weather?q=$cityName&'
       'units=metric&appid=${AppConstants.weatherApiKey}';

   var result = await _getWeather(url);

   print("------- current weather request -----------");

    return result;
  }

  static Future<Map<String, dynamic>> getHourlyWeather(CoordinateModel coords) async
  {
    final String url =
        'https://api.openweathermap.org/data/2.5/onecall?lat=${coords.latitude}&lon=${coords.longitude}'
        '&exclude=current,minutely,daily,alerts&units=metric&appid=${AppConstants.weatherApiKey}';

    var result = await _getWeather(url);

    print("------- hourly weather request -----------");

    return result;
  }

  static Future<Map<String, dynamic>> getDailyWeather(CoordinateModel coords) async
  {
    final String url =
        'https://api.openweathermap.org/data/2.5/onecall?lat=${coords.latitude}&lon=${coords.longitude}'
        '&exclude=current,minutely,hourly,alerts&units=metric&appid=${AppConstants.weatherApiKey}';

    var result = await _getWeather(url);

    print("------- daily weather request -----------");

    return result;
  }

  static Future<Map<String, dynamic>> _getWeather(String url) async
  {
    Map<String, dynamic> result;

    try {
      var response = await get(url);
      result = response.statusCode == 200 ? jsonDecode(response.body) : null;
    }
    catch(exception) {
      print(exception);
    }

    return result;
  }
}